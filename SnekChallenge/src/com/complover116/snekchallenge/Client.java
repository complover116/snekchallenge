package com.complover116.snekchallenge;

import java.io.IOException;
import java.net.Socket;

public class Client implements Runnable {

    String address;
    SnekPanel snekPanel;

    Client(String address, SnekPanel snekPanel) {
        this.address = address;
        this.snekPanel = snekPanel;
    }
    @Override
    public void run() {
        try {
            Socket connection = new Socket(address, 25565);
            snekPanel.connection = connection;
            while (true) {
                Protocol.getTickInfo(connection, snekPanel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
