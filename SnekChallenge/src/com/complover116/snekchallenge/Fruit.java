package com.complover116.snekchallenge;

import java.util.ArrayList;
import java.util.Random;

public class Fruit {
    int x;
    int y;
    boolean dead = false;
    Random r = new Random();
    public Fruit(int x, int y) {
        this.x = x;
        this.y = y;
    }
    void respawn(ArrayList<Player> players) {
        this.x = r.nextInt(49);
        this.y = r.nextInt(49);
        this.dead = false;
    }
}
