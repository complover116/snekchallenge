package com.complover116.snekchallenge;

import java.util.ArrayList;

class Snake {
    static class Segment {
        int x;
        int y;
        Segment(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    enum Direction {
        UP, DOWN, LEFT, RIGHT
    }
    int[] spawnX = {5, 44, 5, 44};
    int[] spawnY = {5, 44, 44, 5};
    ArrayList<Segment> segments = new ArrayList<>();
    volatile Direction direction = Direction.RIGHT;
    Direction lastDirection = Direction.RIGHT;

    boolean dead = false;
    boolean gainedSegment = false;
    Snake(int id) {
        int x = spawnX[id];
        int y = spawnY[id];
        for (int i = 0; i < 5; i ++)
            segments.add(new Segment(x, y));
    }

    Segment getNewCoords() {
        int x = 0;
        int y = 0;
        if (direction == Direction.RIGHT) {
            x = segments.get(0).x + 1;
            y = segments.get(0).y;
        } else if (direction == Direction.LEFT) {
            x = segments.get(0).x - 1;
            y = segments.get(0).y;
        } else if (direction == Direction.DOWN) {
            x = segments.get(0).x;
            y = segments.get(0).y + 1;
        } else {
            x = segments.get(0).x;
            y = segments.get(0).y - 1;
        }
        if (x > 49) {
            x = x - 50;
        }
        if (x < 0) {
            x = 50 + x;
        }
        if (y > 49) {
            y = y - 50;
        }
        if (y < 0) {
            y = 50 + y;
        }
        return new Segment(x, y);
    }
    boolean tick(ArrayList<Player> players, Fruit fruit) {
        if (dead) {
            updateSegments(-10, -10);
            return false;
        }
        Segment newCoords = getNewCoords();
        for (Player player : players) {
            for (Segment segment : player.snake.segments) {
                if (segment.x == newCoords.x && segment.y == newCoords.y) {
                    return false;
                }
            }
        }
        if (newCoords.x == fruit.x && newCoords.y == fruit.y) {
            fruit.dead = true;
            this.gainedSegment = true;
            Segment lastSegment = this.segments.get(this.segments.size()-1);
            this.segments.add(new Segment(lastSegment.x, lastSegment.y));
            this.segments.add(new Segment(lastSegment.x, lastSegment.y));
        }
        updateSegments(newCoords.x, newCoords.y);
        return true;
    }

    void updateSegments(int x, int y) {
        lastDirection = direction;
        for (int i = segments.size() - 1; i > 0; i --) {
            segments.get(i).x = segments.get(i-1).x;
            segments.get(i).y = segments.get(i-1).y;
        }
        segments.get(0).x = x;
        segments.get(0).y = y;
    }

}
