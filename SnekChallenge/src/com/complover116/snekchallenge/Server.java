package com.complover116.snekchallenge;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements Runnable {
    ArrayList<Player> players = new ArrayList<>();
    Fruit fruit = new Fruit(25, 25);
    @Override
    public void run() {
        System.out.println("Starting server thread");
        try {
            ServerSocket serverSocket = new ServerSocket(25565);
            for (int i = 0; i < Main.PLAYER_COUNT; i ++) {
                System.out.println("Waiting for player "+i);
                Socket connection = serverSocket.accept();
                Player player = new Player(connection, new Snake(i));
                players.add(player);
                new Thread(new ServerInputThread(player), "Server Input Thread "+i).start();
            }
            System.out.println("Game started!");
            while (true) {
                Thread.sleep(100);
                for (Player player : players) {
                    synchronized (player.snake) {
                        if (!player.snake.tick(players, fruit)) {
                            player.snake.dead = true;
                        }
                    }
                }
                if (fruit.dead) {
                    fruit.respawn(players);
                }

                for (Player player : players) {
                    Protocol.sendTickInfo(players, fruit, player.connection);
                }
                for (Player player : players) {
                    player.snake.gainedSegment = false;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }
}
