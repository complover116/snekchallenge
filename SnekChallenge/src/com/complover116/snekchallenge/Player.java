package com.complover116.snekchallenge;

import java.net.Socket;

public class Player {
    final Snake snake;
    Socket connection;

    public Player(Socket socket, Snake snake) {
        connection = socket;
        this.snake = snake;
    }
}
