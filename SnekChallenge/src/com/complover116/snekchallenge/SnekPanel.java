package com.complover116.snekchallenge;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.Socket;
import java.util.ArrayList;


public class SnekPanel extends JPanel implements KeyListener {
    static final Color BACKGROUND_COLOR = new Color(0, 0, 0);
    static final Color CELL_COLOR = new Color(255, 255, 255);
    static final Color SNEK1_COLOR = new Color(255, 0, 0);
    static final Color SNEK2_COLOR = new Color(0, 0, 255);
    static final Color SNEK3_COLOR = new Color(0, 255, 0);
    static final Color FRUIT_COLOR = new Color(255, 150, 0);

    final ArrayList<Snake> snakes = new ArrayList<Snake>();
    final Fruit fruit = new Fruit(25, 25);

    Socket connection;

    SnekPanel() {
        for (int i = 0; i < Main.PLAYER_COUNT; i ++){
            snakes.add(new Snake( i));
        }
        this.addKeyListener(this);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g.setFont(new Font("TimesRoman", Font.PLAIN, 50));
        g2d.setBackground(BACKGROUND_COLOR);
        g2d.clearRect(0, 0, getWidth(), getHeight());
        g2d.setColor(CELL_COLOR);
        for (int i = 1; i < 51; i ++) {
            g2d.drawLine(i*10, 0, i*10, 500);
        }
        for (int i = 1; i < 51; i ++) {
            g2d.drawLine(0,i*10, 500, i*10);
        }
        synchronized (snakes) {
            for (int i = 0; i < snakes.size(); i++) {
                if (i == 0) {
                    g2d.setColor(SNEK1_COLOR);
                } else if (i == 1) {
                    g2d.setColor(SNEK2_COLOR);
                } else {
                    g2d.setColor(SNEK3_COLOR);
                }
                for (Snake.Segment segment : snakes.get(i).segments) {
                    g2d.fillRect(segment.x * 10, segment.y * 10, 10, 10);
                }
                g2d.drawString(""+snakes.get(i).segments.size(), 50 + 100*i, 540);
            }
            g2d.setColor(FRUIT_COLOR);
            g2d.fillRect(fruit.x * 10, fruit.y * 10, 10, 10);
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("Key pressed "+e.getKeyCode());
        if (connection == null) {
            return;
        }

        Protocol.sendKeyPress(connection, e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
